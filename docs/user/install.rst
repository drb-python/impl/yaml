.. _install:

Installation of yaml implementation
====================================
Installing ``drb-driver-yaml`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-yaml
