.. _limitation:

Limitations
-----------
The current version does not manage child modification and insertion. YamlNode
is currently read only.
Currently, DRB project is not tested on Windows environment and some issues may
appear on Windows systems. If any troubleshoot occurred with the DRB library,
please report us, opening a new accident on the project GitLab page.