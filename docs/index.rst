===================
Data Request Broker
===================
--------------------
Yaml driver for DRB
--------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-yaml/month
    :target: https://pepy.tech/project/drb-driver-yaml
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-yaml.svg
    :target: https://pypi.org/project/drb-driver-yaml/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-yaml.svg
    :target: https://pypi.org/project/drb-driver-yaml/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-yaml.svg
    :target: https://pypi.org/project/drb-driverF-yaml/
    :alt: Python Version Support Badge

-------------------


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

Others
======
.. toctree::
   :maxdepth: 2

   user/limitation
