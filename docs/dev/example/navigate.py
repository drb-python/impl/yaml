from drb.drivers.yaml import YamlNode

path = 'tests/resources/geo.yml'

node = YamlNode(path)

# print FeatureCollection
print(node['type'].value)

# print Point
print(node['features']['geometry']['type'].value)

# print [102, 0]
print(node['features', 1]['geometry']['coordinates'].value)

# print [105, 1]
print(node['features', 1]['geometry']['coordinates', 3].value)

# {'type': 'Feature', 'geometry': {'type': 'Point', 'coordinates': [102, 0.5]}, 'properties': {'prop0': 'value0'}}
print(node['features'].value)
