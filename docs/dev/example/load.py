from drb.drivers.file import DrbFileFactory

from drb.drivers.yaml import YamlNodeFactory

path = 'path/to/file.yml'

# use path
node = YamlNodeFactory().create(path)

# use fileNode
file_node = DrbFileFactory().create(path)

node = YamlNodeFactory().create(file_node)


