.. _example:

Example
=======

Load a yaml file
------------------
.. literalinclude:: example/load.py
    :language: python

Navigate in a YamlNode
-----------------------
.. literalinclude:: example/navigate.py
    :language: python
